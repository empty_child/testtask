/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { RunScriptWebpackPlugin } = require('run-script-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = [
    {
        mode: 'development',
        devtool: 'cheap-module-source-map',
        output: {
            filename: 'server.js',
            path: path.resolve(__dirname, 'build', 'dev'),
            publicPath: 'assets/',
        },
        entry: {
            server: [
                'webpack/hot/poll?1000',
                path.join(__dirname, 'src', 'server', 'index.ts'),
            ],
        },
        target: 'node',
        plugins: [
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: ['**/*', 'server*'],
            }),
            new CopyPlugin({
                patterns: [
                    {
                        from: path.join(__dirname, 'public'),
                        to: path.join(__dirname, 'build', 'dev', 'assets'),
                    },
                ],
            }),
            new webpack.HotModuleReplacementPlugin(),
            new RunScriptWebpackPlugin({
                name: 'server.js',
                nodeArgs: ['--inspect'],
            }),
            new ForkTsCheckerWebpackPlugin({
                typescript: {
                    diagnosticOptions: {
                        semantic: true,
                        syntactic: true,
                    },
                },
                eslint: {
                    files: './src/**/*.{ts,tsx,js,jsx}',
                },
            }),
        ],
        module: {
            strictExportPresence: true,
            rules: [
                {
                    test: /\.(ts|tsx|js|jsx)$/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            sourceType: 'unambiguous',
                        },
                    },
                    exclude: [
                        '/node_modules/',
                        /\bcore-js\b/,
                        /\bwebpack\/buildin\b/,
                        /@babel\/runtime-corejs3/,
                    ],
                },
            ],
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
            modules: [
                path.resolve(`${__dirname}/src`),
                path.resolve(`${__dirname}/node_modules`),
            ],
        },
        externals: [nodeExternals({ allowlist: ['webpack/hot/poll?1000'] })],
    },
];
