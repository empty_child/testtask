/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const webpack = require('webpack');
const chalk = require('chalk');
const WebpackDevServer = require('webpack-dev-server');
const { choosePort } = require('react-dev-utils/WebpackDevServerUtils');
const clientConfig = require('../../webpack.config.client.dev');
const serverConfig = require('../../webpack.config.server.dev');
const statsOptions = require('./stats-options');

const clientCompiler = webpack(clientConfig);
const serverCompiler = webpack(serverConfig);

// const clientDevServer = new WebpackDevServer(clientCompiler, {
//     stats: statsOptions,
//     port: 8080,
//     quiet: false,
//     inline: true,
//     overlay: true,
//     publicPath: '/',
//     historyApiFallback: true,
//     contentBase: path.join(__dirname, 'build', 'dev'),
//     writeToDisk: true,
//     proxy: {
//         '/assets/**': {
//             target: 'http://localhost:3000',
//             bypass: (req) => {
//                 const assetsRoot = path.resolve('/assets');

//                 if (req.url.startsWith(assetsRoot)) {
//                     return req.url;
//                 }

//                 return null;
//             },
//         },
//     },
// });

// serverCompiler.hooks.done.tap('compile', () =>
//     console.log('Compiling server...'));
// serverCompiler.hooks.done.tap('invalid', () =>
//     console.log('Compiling server...'));
// serverCompiler.hooks.done.tap('done', () =>
//     console.log(chalk.cyan('Server ready')));

clientCompiler.hooks.done.tap('invalid', () =>
    console.log('Compiling client...'));
clientCompiler.hooks.done.tap('done', () =>
    console.log(chalk.cyan('Client ready')));

const DEFAULT_PORT = 8080;
const HOST = '0.0.0.0';

// We attempt to use the default port but if it is busy, we offer the user to
// run on a different port. `detect()` Promise resolves to the next free port.
choosePort(HOST, DEFAULT_PORT)
    .then((port) => {
        if (!port) {
            // We have not found a port.
            return;
        }
        serverCompiler.watch(100, () => {});

        // clientDevServer.listen(port, HOST, () => {
        //     console.log(
        //         `Client dev server running at http://${HOST}:${port}...`,
        //     );
        // });
    })
    .catch((err) => {
        if (err && err.message) {
            console.log(err.message);
        }
        process.exit(1);
    });
