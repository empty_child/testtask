import React, { PropsWithChildren } from 'react';
import { Grommet, Box } from 'grommet';
import { grommet } from 'grommet/themes';
import Header from 'components/header/header';
import Footer from 'components/footer/footer';

export default function App(props: PropsWithChildren<unknown>) {
    const { children } = props;
    return (
        <>
            <Grommet theme={grommet} full>
                <Box fill flex direction="column">
                    <Header />
                    <Box flex={{ grow: 1, shrink: 0 }}>{children}</Box>
                    <Footer />
                </Box>
            </Grommet>
        </>
    );
}
