import React from 'react';
import { Text, Footer as FooterComponent } from 'grommet';

export default function Footer() {
    return (
        <>
            <FooterComponent
                background="dark-3"
                pad="small"
                justify="center"
                margin={{ top: 'small' }}
            >
                <Text>2020 - RushQA - Test Build</Text>
            </FooterComponent>
        </>
    );
}
