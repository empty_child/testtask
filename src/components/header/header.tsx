import { getCurrentUser, logout } from 'api/login';
import React, { useCallback, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {
    Header as HeaderComponent, Image, Box, Anchor, Avatar,
} from 'grommet';
import { AnchorLink } from 'components/anchor-link/anchor-link';
import { useStateContext } from 'store/store';
import { UserCases } from 'store/reducers/user';

export default function Header() {
    const [usernameState, setUsernameState] = useState('');
    const { state, dispatch } = useStateContext();
    useEffect(() => {
        const { username } = getCurrentUser();
        setUsernameState(username);

        if (dispatch) dispatch({ type: UserCases.SET_IS_USER_UPDATED, payload: false });
    }, [dispatch, state.user.isUserUpdated]);

    const logoutUser = useCallback(() => {
        logout();
        if (dispatch) dispatch({ type: UserCases.SET_IS_USER_UPDATED, payload: true });
    }, [dispatch]);

    return (
        <>
            <HeaderComponent background="dark-3" fill="horizontal" pad="small">
                <Box
                    round="full"
                    as={({
                        directionProp,
                        elevationProp,
                        fillProp,
                        overflowProp,
                        wrapProp,
                        widthProp,
                        heightProp,
                        responsive,
                        ...p
                    }) => <Link {...p} to="/" />}
                >
                    <Image height="40px" src="/assets/logo.png" />
                </Box>
                {usernameState ? (
                    <>
                        <Box direction="row" align="center">
                            <Avatar
                                src="//s.gravatar.com/avatar/b7fb138d53ba0f573212ccce38a7c43b?s=80"
                                size="40px"
                            />
                            <Box pad={{ left: 'small' }}>
                                <AnchorLink to="/login" label={usernameState} />
                            </Box>
                            <Box pad={{ left: 'small' }}>
                                <Anchor
                                    label="Выйти"
                                    onClick={() => logoutUser()}
                                />
                            </Box>
                        </Box>
                    </>
                ) : (
                    <Box direction="row" align="center">
                        <AnchorLink to="/login" label="Войти" />
                    </Box>
                )}
            </HeaderComponent>
        </>
    );
}
