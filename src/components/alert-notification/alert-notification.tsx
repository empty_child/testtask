import React from 'react';
import { Alert, Close } from 'grommet-icons';
import { Box, Text } from 'grommet';

export default function AlertNotification({
    message,
    onClose,
}: {
    message: string;
    onClose: () => void;
}) {
    return (
        <Box fill="horizontal" background="status-warning">
            <Box
                pad="medium"
                direction="row"
                gap="medium"
                align="center"
                justify="center"
            >
                <Alert />
                <Text>{message}</Text>
                <Close onClick={onClose} />
            </Box>
        </Box>
    );
}
