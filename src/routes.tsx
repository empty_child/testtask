import React from 'react';
import { Route, Switch } from 'react-router-dom';

import App from 'components/app/app';
import FirstPage from 'pages/first-page/first-page';
import ErrorPage from 'pages/error-page/error-page';
import LoginPage from 'pages/login-page/login-page';
import { StateProvider } from 'store/store';

const WrappedComponent = (Component: React.ComponentType, props: any) => (
    <StateProvider>
        <App>
            <Component {...props} />
        </App>
    </StateProvider>
);

export default (
    <Switch>
        <Route
            exact
            path="/"
            render={(props) => WrappedComponent(FirstPage, props)}
        />
        <Route
            exact
            path="/login"
            render={(props) => WrappedComponent(LoginPage, props)}
        />
        <Route render={() => <ErrorPage error="404" />} />
    </Switch>
);
