import { Express } from 'express';
import isEmpty from 'lodash/isEmpty';
import difference from 'lodash/difference';
import keys from 'lodash/keys';
import omit from 'lodash/omit';
import uniq from 'lodash/uniq';
import { ExtraFields, FileSystem, FileTypes } from 'models';
import { ContentTypes } from 'models/file-types.model';
import { Types } from 'mongoose';
import flatten from 'lodash/flatten';

type CopyCutPostBody = {
    ids: Array<string>;
};

type PastePostBody = {
    pasteTo: string;
};

type CreatePostBody = {
    leafTypeId: string;
    name: string;
    dependsOn: string;
    content?: string;
    contentType?: ContentTypes;
    size?: number;
    extraFields?: { [x: string]: string };
};

type CreateTypePostBody = {
    fileType: string;
    contentType: ContentTypes;
    isAbleToEdit: boolean;
    extraFields: { [x: string]: string };
};

type EditTypePostBody = {
    content: string;
};

export default function editOperations(app: Express) {
    const transferIdsStorage: Types.ObjectId[] = [];
    let operation: string;

    // Copy or cut, save files ids
    app.post('/copy|cut', (req, res) => {
        const { body, path }: { body: CopyCutPostBody; path: string } = req;
        operation = path;

        if (!body || isEmpty(body.ids)) {
            res.status(500).send({
                message: 'ids field is empty',
            });
            return;
        }

        FileSystem.find(
            { _id: { $in: body.ids.map((id) => new Types.ObjectId(id)) } },
            { _id: 1 },
        )
            .then((filesystem) => {
                filesystem.map((file) => transferIdsStorage.push(file.id));
                res.sendStatus(200);
            })
            .catch((err) => {
                transferIdsStorage.length = 0;
                res.status(500).send({ message: err.message });
            });
    });

    // Paste files by saved ids
    app.post('/paste', async (req, res) => {
        const { body }: { body: PastePostBody } = req;
        console.log('transferIdsStorage', transferIdsStorage);

        if (!body || isEmpty(body.pasteTo)) {
            res.status(500).send({
                message: 'Destination field is empty',
            });
            return;
        }

        if (isEmpty(transferIdsStorage)) {
            res.status(500).send({
                message: 'Nothing to replace',
            });
            return;
        }

        const fsBranch = await FileSystem.find({
            _id: {
                $in: transferIdsStorage.map((id) => new Types.ObjectId(id)),
            },
        });

        if (
            !isEmpty(
                await FileSystem.find({
                    dependsOn: new Types.ObjectId(body.pasteTo),
                    name: {
                        $in: fsBranch.map((file) => file.name),
                    },
                }),
            )
        ) {
            res.status(500).send({ message: 'Names collision' });
            return;
        }

        try {
            if (operation === '/cut') {
                await FileSystem.updateMany(
                    {
                        _id: {
                            $in: transferIdsStorage.map(
                                (id) => new Types.ObjectId(id),
                            ),
                        },
                    },
                    { $set: { dependsOn: new Types.ObjectId(body.pasteTo) } },
                );

                res.sendStatus(200);
                transferIdsStorage.length = 0;
                operation = '';
                return;
            }
            await FileSystem.insertMany(
                fsBranch.map((file) => {
                    file.set('dependsOn', new Types.ObjectId(body.pasteTo));
                    file.set('_id', new Types.ObjectId());
                    file.set('isNew', true);
                    file.set('creationDate', Date.now());
                    console.log(file);

                    return file;
                }),
            );

            res.sendStatus(200);
            transferIdsStorage.length = 0;
            operation = '';
            return;
        } catch (err) {
            res.status(500).send({ message: err.message });
        }
    });

    // Create leaf in filesystem
    app.post('/create', async (req, res) => {
        try {
            const { body }: { body: CreatePostBody } = req;

            if (
                body
                && !isEmpty(body.leafTypeId)
                && !isEmpty(body.name)
                && !isEmpty(body.dependsOn)
            ) {
                const fileType = await FileTypes.findById(
                    new Types.ObjectId(body.leafTypeId),
                );

                const extraFields = await ExtraFields.findById(
                    new Types.ObjectId(fileType?.extraFields),
                );

                const fileExistance = await FileSystem.findOne({
                    dependsOn: new Types.ObjectId(body.dependsOn),
                    name: body.name,
                });

                if (isEmpty(fileExistance)) {
                    FileSystem.insertMany([
                        {
                            type: new Types.ObjectId(fileType?.id),
                            name: body.name,
                            dependsOn: new Types.ObjectId(body.dependsOn),
                            ...(fileType?.fileType !== 'folder' && {
                                content: body.content,
                                size: body.size,
                                extraFields: omit(
                                    body.extraFields,
                                    difference(
                                        keys(body.extraFields),
                                        keys(extraFields?.fields),
                                    ),
                                ),
                            }),
                        },
                    ]);

                    res.sendStatus(200);
                    return;
                }
                res.status(500).send({
                    message: 'Incorrect request (names collision)',
                });
                return;
            }
            res.status(500).send({
                message: 'Incorrect request (insufficient data)',
            });
        } catch (err) {
            res.status(500).send({ message: err.message });
        }
    });

    // Create type for filesystem
    app.post('/createType', async (req, res) => {
        try {
            const { body }: { body: CreateTypePostBody } = req;
            if (body && !isEmpty(body.fileType) && !isEmpty(body.contentType)) {
                const fileType = await FileTypes.findOne({
                    fileType: body.fileType,
                });

                if (fileType?.fileType) {
                    res.status(500).send({ message: 'Collision detected' });
                    return;
                }
                const fields = await ExtraFields.insertMany([
                    { fields: body.extraFields },
                ]);

                FileTypes.insertMany([
                    {
                        fileType: body.fileType,
                        contentType: body.contentType,
                        isAbleToEdit: body.isAbleToEdit || false,
                        extraFields: fields[0].id,
                    },
                ]);
                res.sendStatus(200);
            } else {
                res.status(500).send({
                    message: 'Incorrect request (insufficient data)',
                });
            }
        } catch (err) {
            res.status(500).send({ message: err.message });
        }
    });

    // Edit file content
    app.post('/edit/:id', async (req, res) => {
        try {
            const { body }: { body: EditTypePostBody } = req;

            const leaf = await FileSystem.findOne({
                _id: new Types.ObjectId(req.params.id),
            });

            if (isEmpty(leaf)) {
                res.status(500).send({ message: 'File not found' });
                return;
            }

            if (!(await FileTypes.findById(leaf?.type))?.isAbleToEdit) {
                res.status(500).send({ message: 'You cannot edit this file' });
                return;
            }

            await FileSystem.updateOne(
                {
                    _id: new Types.ObjectId(req.params.id),
                },
                {
                    $set: {
                        content: body.content,
                        lastModificationDate: Date.now(),
                    },
                },
            );
            res.sendStatus(200);
        } catch (err) {
            res.status(500).send({ message: err.message });
        }
    });

    // Remove file by id
    app.delete('/remove/:id', async (req, res) => {
        try {
            const filesForRemoving: any[] = [];
            const recursiveSearch = async (ids: any[]) => {
                if (isEmpty(ids)) return;
                const result = await FileSystem.find(
                    {
                        dependsOn: {
                            $in: ids.map((id) => new Types.ObjectId(id.id)),
                        },
                    },
                    { id: 1 },
                );

                await recursiveSearch(result);
                filesForRemoving.push(result.map((elem) => elem.id));
            };
            const file = await FileSystem.findById(
                new Types.ObjectId(req.params.id),
            );
            const type = await FileTypes.findById(file?.type);
            if (type?.fileType === 'folder') {
                await recursiveSearch([{ id: req.params.id }]);
                await FileSystem.deleteMany({
                    _id: {
                        $in: uniq(flatten(filesForRemoving)).map(
                            (rmfile) => new Types.ObjectId(rmfile),
                        ),
                    },
                });
            } else {
                await FileSystem.findOneAndRemove({
                    _id: new Types.ObjectId(req.params.id),
                });
            }
            res.sendStatus(200);
        } catch (err) {
            res.status(500).send({ message: err.message });
        }
    });
}
