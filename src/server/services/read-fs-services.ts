import { Express } from 'express';
import { Types } from 'mongoose';
import { FileSystem, FileTypes } from 'models';
import isEmpty from 'lodash/isEmpty';

export default function readOperations(app: Express) {
    // Get all filesystem tree
    app.get('/list', (req, res) => {
        FileSystem.find({})
            .then((fileSystem) => {
                res.status(200).send(fileSystem);
            })
            .catch((err) => {
                res.status(500).send({ message: err.message });
            });
    });

    // Get file/folder with files by id
    app.get('/leaf/:id', async (req, res) => {
        try {
            const leaf = await FileSystem.findOne({
                _id: new Types.ObjectId(req.params.id),
            });
            const typeInfo = await FileTypes.find();

            if (!leaf || isEmpty(typeInfo)) {
                res.status(500).send({
                    message: `${!leaf ? 'leaf with this id not found ' : ''} ${
                        !typeInfo ? 'Cannot read typeInfo' : ''
                    }`,
                });
                return;
            }

            const recursiveSearch = async (id: string) => {
                const result = await FileSystem.find({
                    dependsOn: new Types.ObjectId(id),
                });

                if (isEmpty(result)) return [];
                const data: any = await Promise.all(
                    result.map(async (elem) => {
                        const currentType = typeInfo.filter(
                            (type) => String(type.id) === String(elem.type),
                        )[0];

                        if (currentType.fileType === 'folder') {
                            return {
                                type: currentType.fileType,
                                name: elem?.name,
                                creationDate: elem?.creationDate,
                                files: await recursiveSearch(elem.id),
                            };
                        }
                        return {
                            type: currentType.fileType,
                            contentType: currentType.contentType,
                            name: elem?.name,
                            creationDate: elem?.creationDate,
                            size: elem?.size,
                            content: elem?.content,
                        };
                    }),
                );
                return data;
            };
            res.status(200).send(await recursiveSearch(req.params.id));
        } catch (err) {
            res.status(500).send({ message: err.message });
        }
    });

    // Get file properties
    app.get('/properties/:id', (req, res) => {
        FileSystem.findOne({ _id: new Types.ObjectId(req.params.id) })
            .then((fileSystem) => {
                res.status(200).send(fileSystem?.extraFields);
            })
            .catch((err) => {
                res.status(500).send({ message: err.message });
            });
    });
}
