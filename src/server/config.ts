if (process.env.BROWSER) {
    throw new Error('Incorrect import `config.js`');
}

export default {
    // Base config
    port: process.env.PORT || 3000,
    mongoDbConnectionString: 'mongodb://localhost:27017/fstesttask',
};
