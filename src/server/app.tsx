/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import express, { NextFunction, Request, Response } from 'express';
import cookieParser from 'cookie-parser';
import chalk from 'chalk';
import cors from 'cors';
import config from './config';

import {
    mongoose, FileSystem, FileTypes, ExtraFields,
} from '../models';
import readOperations from './services/read-fs-services';
import editOperations from './services/edit-fs-operations';

type ExpressError = Error & { status: number };

function App() {
    const app = express();

    function registerNodeMiddleware() {
        const corsOptions = {
            origin: 'http://localhost:8000',
        };

        app.use(cors(corsOptions));
        // Creating a single index route to server our React application from.
        app.use(cookieParser());

        // parse requests of content-type - application/x-www-form-urlencoded
        app.use(express.json());
        // parse requests of content-type - application/json
        app.use(
            express.urlencoded({
                extended: true,
            }),
        );
    }

    function dbInit() {
        function initial() {
            FileTypes.estimatedDocumentCount({}, (error, count) => {
                if (!error && count === 0) {
                    const folder = new FileTypes({
                        fileType: 'folder',
                    });
                    folder.save((err) => {
                        if (err) {
                            console.log('error', err);
                        }

                        console.log(
                            "added 'folder' type to fileTypes collection",
                        );

                        FileSystem.estimatedDocumentCount(
                            {},
                            (fsError, fsCount) => {
                                if (!fsError && fsCount === 0) {
                                    new FileSystem({
                                        name: 'root',
                                        type: folder.id,
                                    }).save((fserr) => {
                                        if (fserr) {
                                            console.log('error', fserr);
                                        }

                                        console.log("added 'root' folder");
                                    });
                                }
                            },
                        );
                    });

                    new ExtraFields({ fields: { lastChanges: '' } }).save(
                        (exerr, result) => {
                            if (exerr) {
                                console.log('error', exerr);
                            }

                            new FileTypes({
                                fileType: 'text',
                                contentType: 'raw',
                                isAbleToEdit: true,
                                extraFields: result.id,
                            }).save((err) => {
                                if (err) {
                                    console.log('error', err);
                                }

                                console.log(
                                    "added 'text' type to fileTypes collection",
                                );
                            });

                            console.log('added extra fields to image type');
                        },
                    );

                    new ExtraFields({ fields: { coordinates: '' } }).save(
                        (exerr, re) => {
                            if (exerr) {
                                console.log('error', exerr);
                            }

                            new FileTypes({
                                fileType: 'images',
                                contentType: 'base64',
                                isAbleToEdit: false,
                                extraFields: re.id,
                            }).save((err) => {
                                if (err) {
                                    console.log('error', err);
                                }

                                console.log(
                                    "added 'images' type to fileTypes collection",
                                );
                            });

                            console.log('added extra fields to image type');
                        },
                    );
                }
            });
        }

        mongoose
            .connect(config.mongoDbConnectionString)
            .then(() => {
                console.log('Successfully connect to MongoDB.');
                mongoose.Promise = global.Promise;

                initial();
            })
            .catch((err) => {
                console.error('Connection error', err);
                process.exit();
            });
    }

    function errorHandling() {
        app.use(
            (
                err: ExpressError,
                _req: Request,
                res: Response,
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                _next: NextFunction,
            ) => {
                console.error(chalk.redBright(String(err.status)));
                res.status(err.status || 500).send({ message: err.message });
            },
        );
    }

    function init() {
        registerNodeMiddleware();
        dbInit();
        readOperations(app);
        editOperations(app);

        errorHandling();
        return app;
    }

    return init();
}

export default App;
