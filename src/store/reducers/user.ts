export type UserState = {
    username: string;
    isUserUpdated: boolean;
};

export const InitialUserState: UserState = {
    username: '',
    isUserUpdated: false,
};

export enum UserCases {
    SET_USERNAME = 'SET_USERNAME',
    SET_IS_USER_UPDATED = 'SET_IS_USER_UPDATED',
}
export type UserAction = (
    | { type: UserCases.SET_USERNAME }
    | {
        type: UserCases.SET_IS_USER_UPDATED;
    }
) & { payload: any };

export default function UserReducer(state: UserState, action: UserAction) {
    switch (action.type) {
        case UserCases.SET_USERNAME:
            return {
                ...state,
                username: action.payload,
            };
        case UserCases.SET_IS_USER_UPDATED:
            return {
                ...state,
                isUserUpdated: action.payload,
            };
        default:
            return state;
    }
}
