import React, { createContext, useContext, useReducer } from 'react';
import combineReducers from './combine-reducers';
import UserReducer, {
    UserAction,
    UserState,
    InitialUserState,
} from './reducers/user';

export type UnitedActions = UserAction;

export type MainState = {
    user: UserState;
};

export type MainReducer = (
    state: MainState,
    action: UnitedActions
) => MainState;

const [combinedReducer, combineInitialState] = combineReducers<MainReducer>({
    user: [UserReducer, InitialUserState],
});

export type StoreType = {
    state: typeof combineInitialState;
    dispatch?: React.Dispatch<UnitedActions>;
};

const store = createContext<StoreType>({ state: combineInitialState });
export const useStateContext = () => useContext(store);
const { Provider } = store;

export function StateProvider({ children }: { children: any }) {
    const [state, dispatch] = useReducer<MainReducer>(
        combinedReducer,
        combineInitialState,
    );
    return <Provider value={{ state, dispatch }}>{children}</Provider>;
}
