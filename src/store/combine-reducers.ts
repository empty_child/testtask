import { Reducer, ReducerState } from 'react';

export default function combineReducers<R extends Reducer<any, any>>(
    reducers: {
        [K in keyof ReducerState<R>]: [
            Reducer<ReducerState<R>[K], any>,
            ReducerState<R>[K],
        ];
    },
): [R, ReducerState<R>] {
    const reducerKeys = Object.keys(reducers);
    const reducerValues = Object.values(reducers);
    let globalState: ReducerState<R>;
    reducerKeys.forEach((key, index) => {
        globalState = { ...globalState, [key]: reducerValues[index][1] };
    });
    let finalReducers: Record<string, R>;
    reducerValues.forEach((value, index) => {
        finalReducers = { ...finalReducers, [reducerKeys[index]]: value[0] };
    });
    return [
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        (state, action) => {
            let hasStateChanged = false;
            const newState: any = {};
            let nextStateForCurrentKey = {};
            for (let i = 0; i < reducerKeys.length; i += 1) {
                const currentKey = reducerKeys[i];
                const currentReducer = finalReducers[currentKey];
                const prevStateForCurrentKey = state[currentKey];
                nextStateForCurrentKey = currentReducer(
                    prevStateForCurrentKey,
                    action,
                );
                hasStateChanged = hasStateChanged
                    || nextStateForCurrentKey !== prevStateForCurrentKey;
                newState[currentKey] = nextStateForCurrentKey;
            }
            return hasStateChanged ? newState : state;
        },
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        globalState,
    ];
}
