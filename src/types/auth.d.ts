export type AuthResponse = {
    id: number;
    username: string;
    email: string;
    accessToken?: string;
};

export type ErrorResponse = {
    message?: string;
};
