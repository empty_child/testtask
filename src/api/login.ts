import axios, { AxiosResponse } from 'axios';
import { AuthResponse, ErrorResponse } from 'types/auth';
import { UserAttributes } from 'types/db';

export async function loginUser(
    userName: string,
    password: string,
): Promise<AxiosResponse<AuthResponse & ErrorResponse>> {
    return axios
        .post<AuthResponse>('http://localhost:3000/login', {
            userName,
            password,
        })
        .then((result) => {
            if (result.data.accessToken) {
                localStorage.setItem('user', JSON.stringify(result.data));
            }
            return result;
        })
        .catch((error) => error.response);
}

export function registerUser(
    email: string,
    userName: string,
    password: string,
): Promise<AxiosResponse<ErrorResponse>> {
    return axios
        .post('http://localhost:3000/register', {
            email,
            userName,
            password,
        })
        .then((result) => result)
        .catch((error) => error.response);
}

export function logout() {
    localStorage.removeItem('user');
}

export function getCurrentUser(): UserAttributes & AuthResponse {
    return JSON.parse(localStorage.getItem('user') || '{}');
}
