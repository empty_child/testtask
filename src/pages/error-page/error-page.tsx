import React from 'react';
import {
    Main, Box, Paragraph, Grommet,
} from 'grommet';
import { grommet } from 'grommet/themes';

export default function ErrorPage({ error }: { error: string }) {
    return (
        <>
            <Grommet theme={grommet} full>
                <Box fill>
                    <Main justify="center" align="center">
                        <Paragraph>{error}</Paragraph>
                    </Main>
                </Box>
            </Grommet>
        </>
    );
}
