import React from 'react';

export default function ErrorPageWithoutStyles({ error }: { error: string }) {
    return (
        <>
            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    textAlign: 'center',
                    height: '90vh',
                }}
            >
                <h3>{`${error} - Ты как сюда залез? Санитары!`}</h3>
            </div>
        </>
    );
}
