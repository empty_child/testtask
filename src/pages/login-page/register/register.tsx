import React, { FormEvent } from 'react';
import {
    Heading, FormField, TextInput, Button, Box, Form,
} from 'grommet';
import AlertNotification from 'components/alert-notification/alert-notification';
import { FormView, FormViewHide } from 'grommet-icons';

type RegisterProps = {
    error: string;
    setError: (_: string) => void;
    email: string;
    setEmail: (_: string) => void;
    login: string;
    setLogin: (_: string) => void;
    password: string;
    setPassword: (_: string) => void;
    isShowPassword: boolean;
    setIsShowPassword: (_: boolean) => void;
    registerUserOnSite: () => void;
};

export default function Register(props: RegisterProps) {
    const {
        error,
        setError,
        email,
        setEmail,
        login,
        setLogin,
        password,
        setPassword,
        isShowPassword,
        setIsShowPassword,
        registerUserOnSite,
    } = props;
    return (
        <>
            <Heading margin="none">Регистрация</Heading>
            {error && (
                <Box
                    justify="center"
                    align="center"
                    pad="small"
                    width="xxlarge"
                >
                    <AlertNotification
                        message={error}
                        onClose={() => setError('')}
                    />
                </Box>
            )}
            <Form>
                <Box pad="small">
                    <FormField htmlFor="email-input-auth">
                        <TextInput
                            id="email-input-auth"
                            placeholder="Электронная почта"
                            type="email"
                            value={email}
                            onInput={(e: FormEvent<HTMLInputElement>) =>
                                setEmail(e.currentTarget.value)}
                        />
                    </FormField>
                    <FormField htmlFor="login-input-auth">
                        <TextInput
                            id="login-input-auth"
                            placeholder="Логин"
                            type="text"
                            value={login}
                            onInput={(e: FormEvent<HTMLInputElement>) =>
                                setLogin(e.currentTarget.value)}
                        />
                    </FormField>
                    <FormField htmlFor="password-input-auth">
                        <Box width="medium" direction="row">
                            <TextInput
                                id="password-input-auth"
                                placeholder="Пароль"
                                type={isShowPassword ? 'text' : 'password'}
                                value={password}
                                plain
                                onInput={(e: FormEvent<HTMLInputElement>) =>
                                    setPassword(e.currentTarget.value)}
                            />
                            <Button
                                icon={
                                    isShowPassword ? (
                                        <FormViewHide />
                                    ) : (
                                        <FormView />
                                    )
                                }
                                onClick={() =>
                                    setIsShowPassword(!isShowPassword)}
                            />
                        </Box>
                    </FormField>
                </Box>
                <Box align="center">
                    <Button
                        type="submit"
                        size="large"
                        primary
                        label="Зарегистрироваться"
                        disabled={!login || !password || !email}
                        onClick={() =>
                            !!login
                            && !!password
                            && !!email
                            && registerUserOnSite()}
                    />
                </Box>
            </Form>
        </>
    );
}
