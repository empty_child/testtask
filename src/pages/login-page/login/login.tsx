import React, { FormEvent } from 'react';
import {
    Heading, FormField, TextInput, Button, Box, Form,
} from 'grommet';
import AlertNotification from 'components/alert-notification/alert-notification';
import { FormView, FormViewHide } from 'grommet-icons';

type LoginProps = {
    error: string;
    setError: (_: string) => void;
    login: string;
    setLogin: (_: string) => void;
    password: string;
    setPassword: (_: string) => void;
    isShowPassword: boolean;
    setIsShowPassword: (_: boolean) => void;
    logInUserOnSite: () => void;
};

export default function Login(props: LoginProps) {
    const {
        error,
        setError,
        login,
        setLogin,
        password,
        setPassword,
        isShowPassword,
        setIsShowPassword,
        logInUserOnSite,
    } = props;
    return (
        <>
            <Heading margin="none">Вход</Heading>
            {error && (
                <Box
                    justify="center"
                    align="center"
                    pad="small"
                    width="xxlarge"
                >
                    <AlertNotification
                        message={error}
                        onClose={() => setError('')}
                    />
                </Box>
            )}
            <Form>
                <Box pad="small">
                    <FormField htmlFor="login-input-signin">
                        <Box width="medium">
                            <TextInput
                                id="login-input-signin"
                                type="text"
                                placeholder="Логин"
                                value={login}
                                plain
                                onInput={(e: FormEvent<HTMLInputElement>) =>
                                    setLogin(e.currentTarget.value)}
                            />
                        </Box>
                    </FormField>
                    <FormField htmlFor="password-input-signin">
                        <Box width="medium" direction="row">
                            <TextInput
                                id="password-input-signin"
                                placeholder="Пароль"
                                type={isShowPassword ? 'text' : 'password'}
                                value={password}
                                plain
                                onInput={(e: FormEvent<HTMLInputElement>) =>
                                    setPassword(e.currentTarget.value)}
                            />
                            <Button
                                icon={
                                    isShowPassword ? (
                                        <FormViewHide />
                                    ) : (
                                        <FormView />
                                    )
                                }
                                onClick={() =>
                                    setIsShowPassword(!isShowPassword)}
                            />
                        </Box>
                    </FormField>
                </Box>
                <Box align="center">
                    <Button
                        type="submit"
                        primary
                        size="large"
                        label="Войти"
                        disabled={!login || !password}
                        onClick={() =>
                            !!login && !!password && logInUserOnSite()}
                    />
                </Box>
            </Form>
        </>
    );
}
