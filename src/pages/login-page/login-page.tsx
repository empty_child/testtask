import { loginUser, registerUser } from 'api/login';
import React, { useCallback, useState } from 'react';
import {
    Main, Anchor, Image, Paragraph,
} from 'grommet';
import { Redirect } from 'react-router-dom';
import { useStateContext } from 'store/store';
import { UserCases } from 'store/reducers/user';
import Login from './login/login';
import Register from './register/register';

export default function LoginPage() {
    const [registerMode, setRegisterMode] = useState(false);
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');

    const [isRedirect, setIsRedirect] = useState(false);
    const [error, setError] = useState('');

    const [isShowPassword, setIsShowPassword] = useState(false);

    const { dispatch } = useStateContext();

    const logInUserOnSite = useCallback(async () => {
        const result = await loginUser(login, password);
        if (result.status === 200) {
            if (dispatch) {
                dispatch({
                    type: UserCases.SET_IS_USER_UPDATED,
                    payload: true,
                });
            }
            setIsRedirect(true);
        } else {
            setError(result.data.message || 'Неизвестная ошибка');
        }
    }, [login, password, dispatch]);

    const registerUserOnSite = useCallback(async () => {
        const result = await registerUser(email, login, password);
        if (result.status === 200) {
            await logInUserOnSite();
            setIsRedirect(true);
        } else {
            setError(result.data.message || 'Неизвестная ошибка');
        }
    }, [email, logInUserOnSite, login, password]);

    return isRedirect ? (
        <Redirect to="/" />
    ) : (
        <>
            <Main justify="center" align="center">
                <Paragraph>
                    <Anchor
                        label="Войти"
                        disabled={!registerMode}
                        onClick={() => {
                            setRegisterMode(false);
                            setError('');
                        }}
                    />
                    {' '}
                    /
                    {' '}
                    <Anchor
                        label="Зарегистрироваться"
                        disabled={registerMode}
                        onClick={() => {
                            setRegisterMode(true);
                            setError('');
                        }}
                    />
                </Paragraph>
                <Image height="100px" width="200px" src="/assets/logo.png" />
                {registerMode ? (
                    <Register
                        error={error}
                        setError={setError}
                        email={email}
                        setEmail={setEmail}
                        login={login}
                        setLogin={setLogin}
                        password={password}
                        setPassword={setPassword}
                        isShowPassword={isShowPassword}
                        setIsShowPassword={setIsShowPassword}
                        registerUserOnSite={registerUserOnSite}
                    />
                ) : (
                    <Login
                        error={error}
                        setError={setError}
                        login={login}
                        setLogin={setLogin}
                        password={password}
                        setPassword={setPassword}
                        isShowPassword={isShowPassword}
                        setIsShowPassword={setIsShowPassword}
                        logInUserOnSite={logInUserOnSite}
                    />
                )}
            </Main>
        </>
    );
}
