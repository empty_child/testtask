import React, { useState } from 'react';
import {
    Main, Heading, Paragraph, Anchor, Text,
} from 'grommet';

export default function FirstPage() {
    const [text, setText] = useState(
        `Господа, убеждённость некоторых оппонентов, в своём классическом представлении,
         допускает внедрение благоприятных перспектив.
         Предварительные выводы неутешительны: современная методология разработки
         способствует повышению качества прогресса профессионального сообщества.
         Кстати, явные признаки победы институционализации подвергнуты целой серии
         независимых исследований.`,
    );
    return (
        <>
            <Main pad="large">
                <Heading>Добро пожаловать!</Heading>
                <Paragraph>
                    Это главная страница тестовой сборки. Развлекайтесь!
                </Paragraph>
                <Text>{text}</Text>
                <Anchor
                    label="Проверка расширяемости страницы"
                    onClick={() => setText((prev) => prev + text)}
                />
            </Main>
        </>
    );
}
