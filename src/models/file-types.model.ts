import { model, Schema, Types } from 'mongoose';

export type ContentTypes = 'raw' | 'base64';

export interface IFileTypes {
    fileType: string;
    contentType: ContentTypes;
    isAbleToEdit: boolean;
    extraFields: Types.ObjectId;
}
const FileTypes = model(
    'FileTypes',
    new Schema<IFileTypes>({
        fileType: {
            type: String,
            required: true,
        },
        contentType: String,
        isAbleToEdit: Boolean,
        extraFields: {
            type: Schema.Types.ObjectId,
            ref: 'ExtraFields',
        },
    }),
);

export default FileTypes;
