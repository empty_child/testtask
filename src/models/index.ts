import mongoose from 'mongoose';
import FileSystem from './fs.model';
import FileTypes from './file-types.model';
import ExtraFields from './extra-fields.model';

export {
    mongoose, FileSystem, FileTypes, ExtraFields,
};
