import { model, Schema } from 'mongoose';

export interface IExtraFields {
    fields: { [x: string]: string };
}

const ExtraFields = model(
    'ExtraFields',
    new Schema<IExtraFields>({
        fields: Schema.Types.Mixed,
    }),
);

export default ExtraFields;
