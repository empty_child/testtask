import { model, Schema, Types } from 'mongoose';
import { IFileTypes } from './file-types.model';

export interface IFileSystem {
    type: IFileTypes;
    name: string;
    creationDate: Date;
    lastModificationDate: Date;
    size: number;
    dependsOn: Types.ObjectId;
    content: any;
    extraFields: { [x: string]: string };
}
const FileSystem = model(
    'FileSystem',
    new Schema<IFileSystem>({
        type: {
            type: Schema.Types.ObjectId,
            ref: 'FileTypes',
        },
        name: {
            type: String,
            required: true,
        },
        creationDate: {
            type: Date,
            default: Date.now,
        },
        lastModificationDate: {
            type: Date,
            default: Date.now,
        },
        size: Number,
        dependsOn: {
            type: Schema.Types.ObjectId,
            // required: true,
        },
        content: Schema.Types.Mixed,
        extraFields: Map,
    }),
);

export default FileSystem;
