const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
// const CompressionPlugin = require('compression-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

module.exports = [
    {
        mode: 'production',
        devtool: 'source-map',
        entry: { server: path.join(__dirname, 'src', 'server', 'index.ts') },
        target: 'node',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'babel-loader',
                    exclude: /node_modules/,
                },
            ],
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
        },
        output: {
            filename: 'server.js',
            path: path.resolve(__dirname, 'build', 'prod'),
        },
        externals: [nodeExternals()],
        optimization: {
            minimize: true,
            minimizer: [
                new TerserPlugin({
                    parallel: true,
                }),
            ],
        },
        plugins: [
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: ['**/*', 'server*'],
            }),
        ],
    },
];
