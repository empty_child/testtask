/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = [
    {
        mode: 'development',
        target: 'web',
        devtool: 'cheap-module-source-map',
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, 'build', 'dev'),
            publicPath: '/assets/',
            chunkFilename: '[name].chunk.js',
        },
        entry: {
            client: [
                'webpack-dev-server/client?http://localhost:8080',
                'webpack/hot/only-dev-server',
                path.join(__dirname, 'src', 'index.tsx'),
            ],
        },
        watch: true,
        plugins: [
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: [
                    '**/*',
                    '!server*',
                    '!assets/**',
                ],
            }),
            new HtmlWebpackPlugin({
                template: './src/pages/main-form.html',
                title: 'RushQA Test Zone',
                templateParameters: {
                    reactcdn: '',
                },
            }),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css',
            }),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.DefinePlugin({
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            }),
            new ForkTsCheckerWebpackPlugin({
                typescript: {
                    diagnosticOptions: {
                        semantic: true,
                        syntactic: true,
                    },
                },
                eslint: {
                    files: './src/**/*.{ts,tsx,js,jsx}',
                },
            }),
        ],
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.jsx'],
            alias: { 'react-dom': '@hot-loader/react-dom' },
            modules: [
                path.resolve(`${__dirname}/src`),
                path.resolve(`${__dirname}/node_modules`),
            ],
        },
        module: {
            strictExportPresence: true,
            rules: [
                {
                    test: /\.(ts|tsx|js|jsx)$/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            sourceType: 'unambiguous',
                        },
                    },
                    exclude: [
                        '/node_modules/',
                        /\bcore-js\b/,
                        /\bwebpack\/buildin\b/,
                        /@babel\/runtime-corejs3/,
                    ],
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1,
                                modules: true,
                            },
                        },
                    ],
                    include: /\.module\.css$/,
                },
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, 'css-loader'],
                    exclude: /\.module\.css$/,
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                        },
                        {
                            loader: 'sass-loader',
                        },
                    ],
                },
                {
                    test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                    type: 'asset/resource',
                },
                {
                    test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                    type: 'asset/inline',
                },
            ],
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendors',
                        chunks: 'all',
                    },
                },
            },
            moduleIds: 'named',
            runtimeChunk: 'single',
        },
    },
];
