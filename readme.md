# FileSystem Test Task

Добрый день
Это тестовый проект файловой системы
К сожалению, я не подрассчитал время, поэтому без интерфейса (но его остатки тут есть, т.к. я использовал свой старый шаблон)

Необходима стандартная mongodb на стандартном порту, иначе перенастроить в папке /src/server/config.ts

Для запуска - yarn start

Имеются следующие команды:
* Чтение:
    * curl http://localhost:3000/list - *общий вид ФС*
    * curl http://localhost:3000/leaf/:id - *получить лист ФС*
    * curl http://localhost:3000/properties/:id - *получить свойства файла*

* Запись:
    * curl -X POST http://localhost:3000/copy -H "Content-Type: application/json" -d '{"ids": ["id1", "id2"]}' - *скопировать файлы*
    * curl -X POST http://localhost:3000/cut -H "Content-Type: application/json" -d '{"ids": ["id1", "id2"]}' - *вырезать файлы*
    * curl -X POST http://localhost:3000/paste -H "Content-Type: application/json" -d '{"pasteTo": "listID"}' - *вставить предварительно вырезанные*
    * curl -X POST http://localhost:3000/create -H "Content-Type: application/json" -d '{
	"leafTypeId":"тип файла",
	"name": "image",
	"dependsOn": "материнская папка",
	"content": "контент",
	"size": размер,
	"extraFields": {
		"coordinates": "123123"
	}
}' - *создать файл*
    * curl -X POST http://localhost:3000/createType -H "Content-Type: application/json" -d '{
	"fileType": "тип файла",
	"contentType": "base64",
	"isAbleToEdit": false,
	"extraFields": {
		"hello": ""
	}
}' - *создать тип*
    * curl -X POST http://localhost:3000/edit/:id -H "Content-Type: application/json" -d '{
	"content": "контент"
}' - *отредактировать файл*
    * curl -X DELETE http://localhost:3000/delete/:id - *удалить файл*
